﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace sp_lesson3
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Example1
            //PrintNumbers();

            //var threads = new Thread[20];
            //for (int i = 0; i < threads.Length; i++)
            //{
            //    threads[i] = new Thread(PrintNumbers);

            //}

            //foreach (var thread in threads)
            //{
            //    thread.Start();
            //    Thread.Sleep(300);
            //} 
            #endregion

            #region Example2
            var thread = new Thread(Sum);
            thread.IsBackground = true;
            thread.Start(new SumArguments { x = 5, y = 10 });
            Console.WriteLine("Main Thread end work");
            #endregion

            //Console.ReadLine();
        }

        static void PrintNumbers()
        {
            var currentThread = Thread.CurrentThread;
            Console.WriteLine($"Thread with id: {currentThread.ManagedThreadId} began work");

            for (int i = 0; i < 10; i++)
            {
                Thread.Sleep(200);
                Console.Write(i + " ");
            }

            Console.WriteLine($"\nThread with id: {currentThread.ManagedThreadId} end work");

            //Console.WriteLine("Start of long work");
            //Thread.Sleep(10000);
            //Console.WriteLine("Work Done");
        }

        static void Sum(object sumArguments)
        {
            Console.WriteLine((sumArguments as SumArguments).x + (sumArguments as SumArguments).y);
            Console.WriteLine("Second Thread end work");
            Thread.Sleep(5000);
        }

    }
}
